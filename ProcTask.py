from mbackup_lib import *
import logging

logger = logging.getLogger(f"__main__.{__name__}")


class ProcTask(object):
	storage = ''
	sub = ''
	ztype = ''
	pattern = '*'
	arch_ext = '.7z'

	files = None
	result_files = None
	files_info = {}

	manager = None

	def get_clone_attrs(self):
		return {'ztype': self.ztype, 'parent': self, 'storage': self.storage, 'sub': self.sub}

	def __init__(self, **kwargs):
		attrs = [x for x in dir(self) if not callable(x) and not(x.startswith('__') and x.endswith('__'))]
		self.__dict__.update((k, v) for k, v in kwargs.items() if k in attrs)

		self.store_path = os.path.normpath(os.path.join(self.storage, self.sub))
		self.temp_path = os.path.normpath(os.path.join(cfg.temp_dir, self.sub))
		self.reco_path = os.path.normpath(os.path.join(cfg.arch_path_recompress, self.sub))

	def init_files(self):
		self.result_files = get_files_info(None)
		if not self.check_files():
			logger.debug(f"Init files from '{self.store_path}' by '{self.pattern}'")
			check_dir(self.store_path)
			self.files = self.filter_files(get_files_info(self.store_path, self.pattern))

	def group_files_quarter(self):
		return self.files.groupby(pd.Grouper(freq='q', key='date'))

	def explode_task_quarter(self):
		if cfg.divide_by_quarter:
			taskdata = self.get_clone_attrs()
			groups = self.group_files_quarter()
			if len(groups) > 1:
				for quarter, files in groups:
					taskdata['files'] = files
					self.manager.add_task(**taskdata)
				self.files = None
				return True
		return False

	def init_files_info(self):
		name = os.path.basename(self.files['path'].get(self.files.first_valid_index(), ''))
		matchmane = re.search(r"(^[a-z_-]+)(\.|\d)", name, re.IGNORECASE)
		self.files_info['basename'] = matchmane.group(1) if matchmane else self.sub.split('/')[-1]
		self.files_info['period'] = self.get_date_period_files(self.files)
		self.files_info['size'] = self.files['size'].sum()
		self.files_info['arch_name'] = os.path.basename(self.files['path'].get(self.files.first_valid_index(), ''))

	@staticmethod
	def filter_files(files):
		if files.empty:
			return files
		return files[(files.date.dt.date > cfg.date_limit_from) & (files.date.dt.date < cfg.date_limit_to)]

	@staticmethod
	def get_date_period_files(files):
		if files.empty:
			return ''
		min_date = files.date.min()
		max_date = files.date.max()
		if min_date.year == max_date.year:
			return f"{min_date:%Y}_{min_date:%m}-{max_date:%m}"
		else:
			return f"{min_date:%Y}_{min_date:%m}-{max_date:%Y}_{max_date:%m}"

	def get_files_date(self):
		return self.files.date.to_list() if self.files is not None else []

	def check_files(self):
		if self.files is None:
			return False
		return not self.files.empty

	def get_ext(self):
		return self.pattern.replace('*', '')
